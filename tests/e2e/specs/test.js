// https://docs.cypress.io/api/table-of-contents

describe('Dashboard', () => {

  describe('Quando acesso o dashboard', () => {

    before(() => {
      cy.visit('/dashboard')
      cy.contains('h4', 'Seu Gerenciador digital de contatos')
    })

    it('Entao devo ver a lista de contato', () => {
      // cy.get('.card').should('have.length', 3)
      cy.get('.card').then((elements) => {
        expect(elements.length > 0).to.be.true
      })
    })
  })
})
